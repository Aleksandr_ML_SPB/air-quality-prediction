FROM python:3.8.7-buster
MAINTAINER Exa Mple "example@example.com"

ENV BASE_DIR="/usr/local/lib/air_quality_prediction"
ENV PYTHONPATH="/usr/local/lib/air_quality_prediction"

COPY pyproject.toml ${BASE_DIR}/
COPY poetry.lock ${BASE_DIR}/

WORKDIR ${BASE_DIR}

RUN pip install poetry
RUN POETRY_VIRTUALENVS_CREATE=false
RUN poetry install --without dev --no-interaction --no-ansi

COPY . ${BASE_DIR}/

ENTRYPOINT ["poetry", "run"]

CMD ["dvc", "repro"]

# in reality if we need to run our project remotely after deploy in docker,
# we need to use bash-script or pass multiply commands together with docker run to
# copy config.dvc.example to .dvc/ (as config file),
# then set sensitive data in configs by commands (for every secret):
# dvc remote modify remote secret_access_key $mySecretFromGitlabCi,
# (like here: https://dvc.org/doc/command-reference/remote/modify)
# then use dvc pull,
# then use dvc force to run update data stage and then we can use dvc repro, probably, then, use dvc push.

# Additionally, our data folders and dvc.lock file should be chained to server folders (or k8s persistent volume)
# to have possibility tracking statuses of training processes and restart from needed point ---
# so, that would be quite complicated bash scripts and infrastructure, and we do not need that for now.