air-quality-prediction
==============================

The main aim of the project is a possibility to predict air quality for target region.
The project is still in development stage.
To build python dependencies one needs to have installed poetry, then go to the
project folder and run in CLI:
```
poetry install
```
After dependencies installation one has to choose new interpreter as IDE interpreter
or activate poetry shell in CLI as:
```
poetry shell
```
Then, in activated shell, run:
```
dvc init
```
Then, one has to copy config.dvc.example to .dvc/, rename file to "config" name and fill up the actual project creds for s3 storage.
Then, run in CLI: 
```
dvc pull
```
And, to run the experiment in dvc dag, run in CLI:
```
dvc repro
```
If your data has changed, or either processing scripts have changed, then the changed
part will be run. If nothing is changed, no dvc dag's stages will run.
After one finishes his work, he has to push changes to dvc repo and to git repo by using CLI:
```
dvc push
git push
```
If some new raw initial data were added (with new names), one has to run in CLI:
```
dvc add filePath
dvc push
git push
```

Master Branch Company is developing that project, the project structure is (might be not updated during developing stage):
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed, also may be data for training.
    │   ├── processed      <- The final, canonical data sets for modeling, also may be logged validated metrics.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- Final project explanation, base docs
    │
    ├── models             <- Temp dir to save, load and validate models, should be cleaned (models are being logging to mlflow)
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── pyproject.toml, 
    │   poetry.lock        <- The poetry file for reproducing the analysis environment, e.g.
    │                         generated with `poetry install`
    │
    ├── docker_for_mlflow_up  <- The dir with docker and docker compose prepared files to run mlflow server with s3 minio, 
    │                            postgres and pgAdmin
    │
    ├── dvc files          <- dvc.yaml with pipelines, params.yaml with vars, dvc.lock with system dvc info
    │
    ├── build, CI/CD, 
    │   deploy files       <- Dockerfile, .gitlab-ci.yml
    │
    ├── config, env files  <- .env.example, config.dvc.example, tox.ini, .gitignore file
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Scripts to download, process or generate data
        │
        ├── external       <- Scripts to update data by using external api
        │
        ├── models         <- Scripts to train and validate models, log to mlflow
        │
        ├── project_dataclasses
        │
        ├── utils          <- Universal functions may be used along the whole project
        │
        └── settings.py    <- Set s3 and mlflow variables


--------