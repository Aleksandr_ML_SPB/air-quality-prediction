from typing import List

import click
import pandas as pd

from ..utils import logger


def get_params_pairs(distances: List, angle_directions: List):
    """
    Get all params pairs of points
    @param distances: list of distances
    @param angle_directions: list of directions
    @return: list of params pairs
    """
    return [(0, 0)] + [(int(dist), int(direction))
                       for dist in distances for direction in angle_directions]


def get_postfix(distance, angle_direction):
    """
    Get prefix by parameters
    @param distance: distance of points
    @param angle_direction: angle of points
    @return: postfix of params
    """
    if distance == 0 and angle_direction == 0:
        return "center"
    return f"{distance}_{angle_direction}"


def get_postfixes(params_pairs):
    """
    Create list of postfixes for given parameters;
    @param params_pairs: values of distances form central point for datasets;
    @return: list of postfixes for all params pairs
    """
    return [get_postfix(distance, angle_direction)
            for distance, angle_direction in params_pairs]


def get_filenames_by_postfixes(input_folder, data_prefix, postfixes):
    """
    @param input_folder: folder with all datasets;
    @param data_prefix: prefix in filename for datasets;
    @param postfixes:
    @return:
    """
    return [input_folder + f"{data_prefix}_{postfix}.csv"
            for postfix in postfixes]


def get_source_filenames(input_folder: str,
                         data_prefix: str,
                         distances: List,
                         angle_directions: List):
    """
    Create list of datasets filenames for given parameters;
    @param input_folder: folder with all datasets;
    @param data_prefix: prefix in filename for datasets;
    @param distances: values of distances form central point for datasets;
    @param angle_directions: values of angle directions for datasets;
    @return: list of filenames with selected input_folder all data_prefix
    for all combination of distances and angle_direction
    """
    filenames = [input_folder + f"{data_prefix}_center.csv"]
    filenames += [input_folder + f"{data_prefix}_{dist}_{direction}.csv"
                  for dist in distances for direction in angle_directions]
    return filenames


def merge_datasets(files_to_merge: List[str], output_file: str) -> None:
    """
    Merge data from files_to_merge list and write it to output_file;
    @param files_to_merge: list of filenames to merge;
    @param output_file: file to write merged data;
    """

    files_number = len(files_to_merge)
    if files_number == 0:
        message = 'nothing to merge'
        logger.error(message)
        raise FileExistsError(message)

    dataframes = [pd.read_csv(filepath, index_col=0, parse_dates=["time"],
                              date_format="%Y-%m-%dT%H:%M") for filepath in
                  files_to_merge]
    result = dataframes[0]
    for idx in range(1, files_number):
        params = files_to_merge[idx].split('_')[-2:]
        postfix = '_'.join(params).split('.', maxsplit=1)[0]
        result = result.merge(
            dataframes[idx], left_on='time', right_on='time',
            how='outer',
            suffixes=("", f"_{postfix}"))
    result.to_csv(output_file, index=False)


@click.command()
@click.argument('input_folder', type=click.Path(exists=True))
@click.argument('output_file_path', type=click.Path())
@click.argument('data_prefix', type=click.STRING)
@click.argument('distances', type=click.UNPROCESSED)
@click.argument('angle_directions', type=click.UNPROCESSED)
def main(input_folder: str,
         output_file_path: str,
         data_prefix: str,
         distances: str,
         angle_directions: str) -> None:
    """
    Merge datasets from folder input_folder for given prefix,
    distances and angle_directions;
    @param input_folder: folder for input datasets;
    @param output_file_path: filepath for merge result;
    @param data_prefix: prefix of datasets;
    @param distances: values of distances form central point for datasets;
    @param angle_directions: values of angle directions for datasets;
    """
    distances_list = distances.split(',')
    angle_directions_list = angle_directions.split(',')
    logger.info("processing %s data", data_prefix)
    filenames = get_source_filenames(input_folder, data_prefix, distances_list,
                                     angle_directions_list)
    merge_datasets(filenames, output_file_path)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
