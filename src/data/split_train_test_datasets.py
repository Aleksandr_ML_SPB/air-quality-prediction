from typing import Tuple

import click
import numpy as np
import pandas as pd

from ..utils import parse_date, create_folder, logger


FEATURE_COLUMNS = [
    "temperature_2m",
    "relativehumidity_2m",
    "pressure_msl",
    "precipitation",
    "windspeed_10m",
    "winddirection_10m"
]
TARGET_COLUMNS = ["pm2_5", "pm10"]


def clear_nans(dataframe: pd.DataFrame, threshold: float) -> pd.DataFrame:
    """
    function dropping nan-filled columns by threshold
    @param dataframe: source df
    @param threshold: nan-filled threshold
    @return: cleaned df
    """
    columns_to_drop = [column for column in dataframe.columns
                       if np.mean(dataframe[column].isnull()) >= threshold
                       and column not in TARGET_COLUMNS]
    dataframe = dataframe.drop(columns=columns_to_drop)
    return dataframe


def split_dataset(dataframe: pd.DataFrame, test_days: int, target_column: str) \
        -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    function splitting df to train and test
    @param dataframe:  source df
    @param test_days: number of test days
    @param target_column: name of target column
    @return:
    """
    test_size = test_days * 24
    dataframe_train = dataframe.iloc[:-test_size]
    dataframe_test = dataframe.iloc[-test_size:]
    features_train = dataframe_train.drop(columns=TARGET_COLUMNS)
    targets_train = dataframe_train[target_column]
    features_test = dataframe_test.drop(columns=TARGET_COLUMNS)
    targets_test = dataframe_test[target_column]
    return features_train, targets_train, features_test, targets_test


@click.command()
@click.argument('input_folder', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path())
@click.argument('test_days', type=click.INT)
@click.argument('nan_threshold', type=click.FLOAT)
def main(input_folder: str,
         output_folder: str,
         test_days: int,
         nan_threshold: float) -> None:
    """
    splits input dataset into train and test datasets
    @param input_folder: folder path with main data to use
    @param output_folder: output folder path
    @param test_days: number of test days
    @param nan_threshold: nan-filled threshold for dropping columns
    """
    logger.info("processing simple splits for universal model")
    forecast_dataframe = pd.read_csv(f"{input_folder}/forecast.csv",
                                     parse_dates=["time"],
                                     date_parser=parse_date,
                                     index_col=0)[FEATURE_COLUMNS]
    air_dataframe = pd.read_csv(f"{input_folder}/air.csv",
                                parse_dates=["time"],
                                date_parser=parse_date,
                                index_col=0)[TARGET_COLUMNS]
    dataframe = forecast_dataframe.merge(air_dataframe,
                                         left_index=True,
                                         right_index=True,
                                         how='outer')
    dataframe = clear_nans(dataframe, nan_threshold)
    dataframe["day_of_week"] = dataframe.index.dayofweek
    dataframe["hour"] = dataframe.index.hour

    for target_number, target_column in enumerate(TARGET_COLUMNS):
        dataframe_not_na_target = dataframe[dataframe[target_column].notna()]
        features_train, targets_train, features_test, targets_test = \
            split_dataset(dataframe_not_na_target, test_days, target_column)
        create_folder(output_folder)
        features_train.to_csv(
            f"{output_folder}/train_features_{target_number}.csv", index=False)
        targets_train.to_csv(
            f"{output_folder}/train_targets_{target_number}.csv", index=False)
        features_test.to_csv(
            f"{output_folder}/test_features_{target_number}.csv", index=False)
        targets_test.to_csv(
            f"{output_folder}/test_targets_{target_number}.csv", index=False)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
