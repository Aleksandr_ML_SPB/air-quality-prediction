from typing import Tuple

import click
import pandas as pd

from ..utils import create_folder, parse_date
from ..utils import logger
from ..utils.columns import column_has_one_of_prefixes
from ..utils.columns import NN_FEATURE_COLUMNS
from ..utils.columns import TARGET_COLUMNS

from ..utils.columns import clear_nans


def split_dataset(dataframe: pd.DataFrame,
                  valid_days: int,
                  test_days: int) \
        -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    function splitting df to train and test
    @param dataframe:  source df
    @param valid_days: number of valid days
    @param test_days: number of test days
    @return: train, valid and test dataframes
    """
    dataframe_size = dataframe.shape[0]

    valid_size = valid_days * 24
    test_size = test_days * 24
    train_size = dataframe_size - valid_size - test_size

    dataframe_train = dataframe.iloc[:train_size]
    dataframe_valid = dataframe.iloc[train_size:train_size + valid_size]
    dataframe_test = dataframe.iloc[train_size + valid_size:]

    return dataframe_train, dataframe_valid, dataframe_test


@click.command()
@click.argument('input_folder', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path())
@click.argument('valid_days', type=click.INT)
@click.argument('test_days', type=click.INT)
@click.argument('nan_threshold', type=click.FLOAT)
def main(input_folder: str,
         output_folder: str,
         valid_days: int,
         test_days: int,
         nan_threshold: float) -> None:
    """
    splits input dataset into train and test datasets
    @param input_folder: folder path with main data to use
    @param output_folder: output folder path
    @param test_days: number of test days
    @param valid_days: number of valid days
    @param nan_threshold: nan-filled threshold for dropping columns
    """
    logger.info("processing simple splits for nn model")
    forecast_dataframe = pd.read_csv(f"{input_folder}/forecast.csv",
                                     parse_dates=["time"],
                                     date_parser=parse_date,
                                     index_col=0)
    forecast_columns = [col for col in forecast_dataframe.columns
                        if column_has_one_of_prefixes(col, NN_FEATURE_COLUMNS)]
    forecast_dataframe = forecast_dataframe[forecast_columns]
    air_dataframe = pd.read_csv(f"{input_folder}/air.csv",
                                parse_dates=["time"],
                                date_parser=parse_date,
                                index_col=0)
    targets = air_dataframe[TARGET_COLUMNS]
    for target_column in TARGET_COLUMNS:
        targets = targets[targets[target_column].notna()]
    dataframe = forecast_dataframe.merge(targets,
                                         left_index=True,
                                         right_index=True,
                                         how='inner')
    dataframe = clear_nans(dataframe, nan_threshold)

    dataframe_train, dataframe_valid, dataframe_test \
        = split_dataset(dataframe, valid_days, test_days)

    create_folder(output_folder)
    dataframe_train.to_csv(output_folder + "train.csv", index=False)
    dataframe_valid.to_csv(output_folder + "valid.csv", index=False)
    dataframe_test.to_csv(output_folder + "test.csv", index=False)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
