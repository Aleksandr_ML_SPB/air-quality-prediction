import requests


def get_coordinates_dict(latitude, longitude):
    """
    Create coordinates dict
    @param latitude: latitude of the point
    @param longitude: longitude of the point
    @return: coordinates dict
    """
    return {
        "latitude": latitude,
        "longitude": longitude,
    }


def get_hourly_dict(hourly_variables):
    """
    Create hourly variables
    @param hourly_variables: hourly variables to create dict
    @return: create dict of hourly variables
    """
    return {
        "hourly": ','.join(hourly_variables)
    }


def get_daily_dict(dayly_variables):
    """
    Create daily variables
    @param dayly_variables: daily variables to create dict
    @return: create dict of daily variables
    """
    return {
        "daily": ','.join(dayly_variables),
        "timezone": "auto",
    }


def get_range_dict(start_date, end_date):
    """
    Create dict of range
    @param start_date: start date
    @param end_date: end date
    @return: dict of range
    """
    start_date = start_date.strftime("%Y-%m-%d")
    end_date = end_date.strftime("%Y-%m-%d")
    return {
        "start_date": start_date,
        "end_date": end_date,
    }


def get_past_forecast_dict(past_days, forecast_days):
    """
    Dict of days count
    @param past_days: past days
    @param forecast_days: forecast days
    @return: dict of days counts
    """
    return {
        "past_days": past_days,
        "forecast_days": forecast_days,
    }


def get_forecast_dict(forecast_days):
    """
    Get dict for forcast days
    @param forecast_days: forecast days
    @return: forecast days dict
    """
    return {
        "forecast_days": forecast_days,
    }


def get_past_dict(past_days):
    """
    Get dict for past days
    @param past_days: past days
    @return: past days dict
    """
    return {
        "past_days": past_days,
    }


def merge_dicts(*dict_args):
    """
    Given any number of dictionaries, shallow copy and merge into a new dict,
    precedence goes to key-value pairs in latter dictionaries.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def send_request(url, params):
    """
    Send request to endpoint
    @param url: endpoints url
    @param params: params of request
    @return: result of request
    """
    res = requests.get(url, params=params)
    if res.status_code != 200:
        raise ValueError(res.text)
    return res.json()
