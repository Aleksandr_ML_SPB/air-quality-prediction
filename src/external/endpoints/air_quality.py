import pandas as pd

from ..common import get_coordinates_dict
from ..common import get_range_dict
from ..common import get_past_dict
from ..common import get_hourly_dict
from ..common import merge_dicts
from ..common import send_request
from ..variables.air_quality import hourly_variables


AIR_QUALITY_ENDPOINT = "https://air-quality-api.open-meteo.com/v1/air-quality"
# domains ?
# cell_selection ?


def get_hourly_air_quality_by_range(latitude, longitude, start_date, end_date):
    """
    Get hourly air quality by range
    @param latitude: latitude of point
    @param longitude: longitude of point
    @param start_date: start date of range
    @param end_date: end date of range
    @return: air quality DataFrame
    """
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_range_dict(start_date, end_date),
                              get_hourly_dict(hourly_variables),)
    response_dict = send_request(AIR_QUALITY_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["hourly"])


def get_hourly_air_quality_by_days_count(latitude, longitude, past_days):
    """
    Get hourly air quality by days count
    @param latitude: latitude of point
    @param longitude: longitude of point
    @param past_days: last days to load
    @return: air quality DataFrame
    """
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_past_dict(past_days),
                              get_hourly_dict(hourly_variables),)
    response_dict = send_request(AIR_QUALITY_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["hourly"])
