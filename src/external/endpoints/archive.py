import pandas as pd
from ..common import get_coordinates_dict
from ..common import get_range_dict
from ..common import get_hourly_dict
from ..common import get_daily_dict
from ..common import merge_dicts
from ..common import send_request
from ..variables.archive import hourly_variables
from ..variables.archive import dayly_variables


ARCHIVE_ENDPOINT = "https://archive-api.open-meteo.com/v1/archive"
# elevation ?
# cell_selection ?
# reanalysis model ?


def get_hourly_archive_by_range(latitude, longitude, start_date, end_date):
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_range_dict(start_date, end_date),
                              get_hourly_dict(hourly_variables), )
    response_dict = send_request(ARCHIVE_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["hourly"])


def get_daily_archive_by_range(latitude, longitude, start_date, end_date):
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_range_dict(start_date, end_date),
                              get_daily_dict(dayly_variables), )
    response_dict = send_request(ARCHIVE_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["daily"])
