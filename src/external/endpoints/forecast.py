import pandas as pd
from ..common import get_coordinates_dict
from ..common import get_range_dict
from ..common import get_past_dict
from ..common import get_forecast_dict
from ..common import get_hourly_dict
from ..common import get_daily_dict
from ..common import merge_dicts
from ..common import send_request

from ..variables.forecast import hourly_variables
from ..variables.forecast import dayly_variables
from ..variables.forecast import get_pressure_level_variables

FORECAST_ENDPOINT = "https://api.open-meteo.com/v1/forecast"
# elevation ?
# models ?
# cell_selection ?


def get_hourly_forecast_by_range(latitude, longitude, start_date, end_date):
    """
    Get hourly forecast
    @param latitude:
    @param longitude:
    @param start_date:
    @param end_date:
    @return: hourly forecast
    """
    pressure_level_variables = get_pressure_level_variables()
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_range_dict(start_date, end_date),
                              get_hourly_dict(hourly_variables +
                                              pressure_level_variables),
                              )
    response_dict = send_request(FORECAST_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["hourly"])


def get_hourly_forecast_by_days_count(latitude,
                                      longitude,
                                      past_days,
                                      forecast_days):
    """
    Get hourly forecast
    @param latitude:
    @param longitude:
    @param past_days:
    @param forecast_days:
    @return: hourly forecast
    """
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_past_dict(past_days),
                              get_forecast_dict(forecast_days),
                              get_hourly_dict(hourly_variables),)
    response_dict = send_request(FORECAST_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["hourly"])


def get_daily_forecast_by_range(latitude,
                                longitude,
                                start_date,
                                end_date):
    """
    Get daily forecast
    @param latitude:
    @param longitude:
    @param start_date:
    @param end_date:
    @return: daily forecast
    """
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_range_dict(start_date, end_date),
                              get_daily_dict(dayly_variables),)
    response_dict = send_request(FORECAST_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["daily"])


def get_daily_forecast_by_days_count(latitude,
                                     longitude,
                                     past_days,
                                     forecast_days):
    """
    Get daily forecast
    @param latitude:
    @param longitude:
    @param past_days:
    @param forecast_days:
    @return: daily forecast
    """
    params_dict = merge_dicts(get_coordinates_dict(latitude, longitude),
                              get_past_dict(past_days),
                              get_forecast_dict(forecast_days),
                              get_daily_dict(dayly_variables),)
    response_dict = send_request(FORECAST_ENDPOINT, params_dict)
    return pd.DataFrame(response_dict["daily"])
