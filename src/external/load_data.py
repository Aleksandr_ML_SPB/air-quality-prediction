import os
import datetime
import geopy.distance
import click
import pandas as pd
from src.external.endpoints.forecast import get_hourly_forecast_by_range
from src.external.endpoints.archive import get_hourly_archive_by_range
from src.external.endpoints.air_quality import get_hourly_air_quality_by_range
from src.data.merge_datasets import get_params_pairs
from src.data.merge_datasets import get_postfixes
from src.data.merge_datasets import get_filenames_by_postfixes
from src.utils.utils import create_folder
from src.utils.utils import read_csv_files
from src.utils.utils import write_csv_files


def get_point(latitude, longitude, kilometers, direction):
    """
    Move point in selected direction
    @param latitude: latitude of point
    @param longitude: longitude of point
    @param kilometers: distance in kilometers
    @param direction: direction to move point
    @return: shifted point
    """
    return geopy.distance.distance(kilometers=kilometers)\
        .destination((latitude, longitude), direction)


@click.command()
@click.argument('raw_folder', type=click.Path())
@click.argument('external_folder', type=click.Path())
@click.argument('latitude', type=click.FLOAT)
@click.argument('longitude', type=click.FLOAT)
@click.argument('distances', type=click.STRING)
@click.argument('angle_directions', type=click.STRING)
def load_data_for_current_date(raw_folder: str,
                               external_folder: str,
                               latitude: float,
                               longitude: float,
                               distances: str,
                               angle_directions: str):
    """
    Save data from external source
    @param raw_folder: folder of raw data
    @param external_folder: folder of external data
    @param latitude: latitude of forcast point
    @param longitude: longitude of forcast point
    @param distances: distances from center to points
    @param angle_directions: angle directions of extra points
    """
    create_folder(external_folder)
    create_folder(external_folder + "forecast/")
    create_folder(external_folder + "archive/")
    create_folder(external_folder + "air_quality/")

    distances_list = distances.split(',')
    angle_directions_list = angle_directions.split(',')

    params_pairs = get_params_pairs(distances_list, angle_directions_list)
    postfixes = get_postfixes(params_pairs)
    forecast_external_filenames = \
        get_filenames_by_postfixes(external_folder + "forecast/",
                                   "forecast", postfixes)
    archive_external_filenames = \
        get_filenames_by_postfixes(external_folder + "archive/",
                                   "archive", postfixes)
    air_quality_external_filenames = \
        get_filenames_by_postfixes(external_folder + "air_quality/",
                                   "air_quality", postfixes)

    if os.listdir(external_folder + "forecast/"):
        forecast_filenames = forecast_external_filenames
        archive_filenames = archive_external_filenames
        air_quality_filenames = air_quality_external_filenames
    else:
        forecast_filenames = \
            get_filenames_by_postfixes(raw_folder + "forecast/",
                                       "forecast", postfixes)
        archive_filenames = \
            get_filenames_by_postfixes(raw_folder + "archive/",
                                       "archive", postfixes)
        air_quality_filenames = \
            get_filenames_by_postfixes(raw_folder + "air_quality/",
                                       "air_quality", postfixes)

    forecast_dataframes = read_csv_files(forecast_filenames)
    archive_dataframes = read_csv_files(archive_filenames)
    air_quality_dataframes = read_csv_files(air_quality_filenames)

    start_datetime = forecast_dataframes[0]["time"].iloc[-1]
    end_datetime = \
        datetime.datetime.now().replace(minute=0, second=0, microsecond=0) \
        - datetime.timedelta(days=2)

    params_pairs = get_params_pairs(distances_list, angle_directions_list)
    for idx, (distance, bearing) in enumerate(params_pairs):
        point = get_point(latitude, longitude, distance, bearing)
        forecast_dataframe = get_hourly_forecast_by_range(
            point.latitude, point.longitude, start_datetime, end_datetime)
        archive_dataframe = get_hourly_archive_by_range(
            point.latitude, point.longitude, start_datetime, end_datetime)
        air_quality_dataframe = get_hourly_air_quality_by_range(
            point.latitude, point.longitude, start_datetime, end_datetime)

        forecast_dataframes[idx] = pd.concat([forecast_dataframes[idx],
                                              forecast_dataframe])
        archive_dataframes[idx] = pd.concat([archive_dataframes[idx],
                                             archive_dataframe])
        air_quality_dataframes[idx] = pd.concat([air_quality_dataframes[idx],
                                                 air_quality_dataframe])
    write_csv_files(forecast_dataframes, forecast_external_filenames)
    write_csv_files(archive_dataframes, archive_external_filenames)
    write_csv_files(air_quality_dataframes, air_quality_external_filenames)


if __name__ == "__main__":
    load_data_for_current_date()
