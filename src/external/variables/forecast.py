main_variables = [
    "latitude",
    "longitude",
    "elevation",  # ?
    "hourly",
    "daily",
    "current_weather",
    "temperature_unit",
    "windspeed_unit",
    "precipitation_unit",
    "timeformat",
    "timezone",
    "past_days",
    "forecast_days",
    "start_date",
    "end_date",
    "models",  # ?
    "cell_selection",  # ?
]

hourly_variables = [
    "temperature_2m",
    "relativehumidity_2m",
    "dewpoint_2m",
    "apparent_temperature",
    "pressure_msl",
    "surface_pressure",
    "cloudcover",
    "cloudcover_low",
    "cloudcover_mid",
    "cloudcover_high",
    "windspeed_10m",
    "windspeed_80m",
    "windspeed_120m",
    "windspeed_180m",
    "winddirection_10m",
    "winddirection_80m",
    "winddirection_120m",
    "winddirection_180m",
    "windgusts_10m",
    "shortwave_radiation",
    "direct_radiation",
    "direct_normal_irradiance",
    "diffuse_radiation",
    "vapor_pressure_deficit",
    "cape",
    "evapotranspiration",
    "et0_fao_evapotranspiration",
    "precipitation",
    "snowfall",
    "precipitation_probability",
    "rain",
    "showers",
    "weathercode",
    "snow_depth",
    "freezinglevel_height",
    "visibility",
    "soil_temperature_0cm",
    "soil_temperature_6cm",
    "soil_temperature_18cm",
    "soil_temperature_54cm",
    "soil_moisture_0_1cm",
    "soil_moisture_1_3cm",
    "soil_moisture_3_9cm",
    "soil_moisture_9_27cm",
    "soil_moisture_27_81cm",
]
pressure_values = [1000, 975, 950, 925, 900, 850, 800,
                   700, 600, 500, 400, 300, 250, 200,
                   150, 100, 70, 50, 30]
pressure_level_variables_prefix = [
    "temperature",
    "relativehumidity",
    "dewpoint",
    "cloudcover",
    "windspeed",
    "winddirection",
    "geopotential_height",
]


def get_pressure_level_variables():
    """
    Get pressure level variables
    @return: pressure level variables list
    """
    res = []
    for prefix in pressure_level_variables_prefix:
        for pressure in pressure_values:
            res.append(f"{prefix}_{pressure}hPa")
    return res


dayly_variables = [
    "temperature_2m_max",
    "temperature_2m_min",
    "apparent_temperature_max",
    "apparent_temperature_min",
    "precipitation_sum",
    "rain_sum",
    "showers_sum",
    "snowfall_sum",
    "precipitation_hours",
    "precipitation_probability_max",
    "precipitation_probability_min",
    "precipitation_probability_mean",
    "weathercode",
    "sunrise",
    "sunset",
    "windspeed_10m_max",
    "windgusts_10m_max",
    "winddirection_10m_dominant",
    "shortwave_radiation_sum",
    "et0_fao_evapotranspiration",
    "uv_index_max",
    "uv_index_clear_sky_max",
]
