import catboost as ct
import numpy as np
import click
import pandas as pd
import mlflow
from mlflow.models.signature import infer_signature
from mlflow import log_metric, log_params
from sklearn.metrics import mean_absolute_error, mean_squared_error

from ..utils import create_folder
from ..utils import logger
from ..project_dataclasses import OutputTrainData
from ..settings import MLFLOW_TRACKING_URI


def train_catboost(x_train: np.ndarray, y_train: np.ndarray,
                   x_test: np.ndarray = np.array([])) -> OutputTrainData:
    """
    the function training baseline catboost regression
    @param x_train: np.ndarray
    @param y_train: np.ndarray
    @param x_test: np.ndarray
    @return: OutputTrainData dataclass
    """
    model = ct.CatBoostRegressor(loss_function="RMSE", random_state=1,
                                 verbose=0, allow_writing_files=False)
    model.fit(x_train, y_train)
    output_train_data = OutputTrainData(model=model)
    if x_test.size:
        output_train_data.predictions_for_test = model.predict(x_test)

    return output_train_data


@click.command()
@click.argument('dataset_folder', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path())
@click.argument('experiment_name', type=click.STRING)
def main(dataset_folder: str, output_folder: str, experiment_name: str) -> None:
    """
    train catboost universal models
    @param experiment_name:
    @param dataset_folder: folder for input datasets;
    @param output_folder: folder to save metrics.
    """
    logger.info("train universal catboost model for both targets")
    create_folder(output_folder)
    for target_number in range(2):
        metrics_dict = {'metrics': ['mae', 'mse']}
        metrics_list = []
        train_features = pd.read_csv(
            f"{dataset_folder}/train_features_{target_number}.csv")
        train_targets = pd.read_csv(
            f"{dataset_folder}/train_targets_{target_number}.csv")
        test_features = pd.read_csv(
            f"{dataset_folder}/test_features_{target_number}.csv")
        test_targets = pd.read_csv(
            f"{dataset_folder}/test_targets_{target_number}.csv")

        output_train_data = \
            train_catboost(x_train=train_features.values,
                           y_train=train_targets.values,
                           x_test=test_features.values)
        signature = infer_signature(test_features,
                                    output_train_data.predictions_for_test)
        model_name = f"ct_target_{target_number}"
        mae = mean_absolute_error(test_targets,
                                  output_train_data.predictions_for_test)
        mse = mean_squared_error(test_targets,
                                 output_train_data.predictions_for_test)
        mlflow.set_experiment(f"{experiment_name}_{target_number}")
        with mlflow.start_run():
            mlflow.catboost.log_model(output_train_data.model,
                                      artifact_path=model_name,
                                      signature=signature,
                                      registered_model_name=model_name)
            log_metric("mae", mae)
            log_metric("mse", mse)
            log_params({"model_type": "ct", "target_number": target_number,
                        "default": True})
            logger.info("model logged to mlflow at uri: %s",
                        MLFLOW_TRACKING_URI)
        metrics_list.extend([mae, mse])
        metrics_dict.update({model_name: metrics_list})
        pd.DataFrame(metrics_dict).to_csv(
            f"{output_folder}/metrics_{model_name}.csv", index=False)
        # later we will have to choose the best model and make final train


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
