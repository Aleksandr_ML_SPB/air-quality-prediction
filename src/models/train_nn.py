import os
import uuid
from typing import Optional, Tuple

import mlflow
import numpy as np
import click
import pandas as pd
from keras import models
from keras.callbacks import ModelCheckpoint, Callback
from mlflow import log_metric, log_metrics, log_params
from mlflow.models import infer_signature
from sklearn.metrics import mean_absolute_error, mean_squared_error

from ..settings import MLFLOW_TRACKING_URI
from ..project_dataclasses import OutputTrainData
from ..models.train_utils import get_rnn_model, get_conv1d_model
from ..utils import logger, create_folder
from ..utils.columns import TARGET_COLUMNS


class CustomMlflowCallback(Callback):
    """
    custom callback for keras with mlflow
    """
    def on_epoch_end(self, epoch, logs=None):
        """
        @param epoch:
        @param logs:
        @return:
        """
        log_metrics({
           "train_loss": logs["loss"],
           "train_mae": logs["mae"],
           "train_mse": logs["mse"],
           "val_loss": logs["val_loss"],
           "val_mae": logs["val_mae"],
           "val_mse": logs["val_mse"]}, step=epoch)


def prepare_nn_data(df: pd.DataFrame, target_number: int,
                    input_deep: int, deep: int,
                    is_only_label: bool = False,
                    means: Optional[np.ndarray] = None,
                    stds: Optional[np.ndarray] = None) -> \
        Tuple[np.ndarray, np.ndarray, Optional[np.ndarray], Optional[
            np.ndarray]]:
    """
    Create X, y, means, stds from DataFrame for given parameters;
    @param df: source df
    @param target_number: label number;
    @param input_deep: deep of X;
    @param deep: deep of forecast;
    @param is_only_label: predict with 1 feature only based on past labels;
    @param means: value of mean;
    @param stds: value of standard deviation
    """

    x_data = []
    y_data = []

    if is_only_label:
        x_part = df[TARGET_COLUMNS[target_number]].copy()
        df, x_part = process_nans(df, x_part)
        means, stds = get_means_and_stds(x_part, means, stds)
        for start, stop in enumerate(range(input_deep, df.shape[0] - deep)):
            x_vector = x_part.iloc[start:stop]
            x_data.append(x_vector.values)
            y_point = df[TARGET_COLUMNS[target_number]].values[stop + deep]
            y_data.append(y_point)
    else:
        x_part = df.drop(columns=TARGET_COLUMNS[target_number])
        df, x_part = process_nans(df, x_part)
        means, stds = get_means_and_stds(x_part, means, stds)
        for start, stop in enumerate(range(input_deep, df.shape[0] - deep)):
            x_matrix = x_part.iloc[start:stop]
            x_data.append(x_matrix)
            y_point = df[TARGET_COLUMNS[target_number]].values[stop + deep]
            y_data.append(y_point)
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    x_data = x_data[pd.notna(y_data)]
    y_data = y_data[pd.notna(y_data)]
    x_data = (x_data - means) / stds

    return x_data, y_data, means, stds


def process_nans(df: pd.DataFrame, x_part: pd.DataFrame) -> \
        Tuple[pd.DataFrame, pd.DataFrame]:
    """
    @param df:
    @param x_part:
    @return:
    """
    x_part = x_part.interpolate(limit_direction='forward')
    x_part = x_part[df.notna().all(axis=1)]
    df = df[df.notna().all(axis=1)]
    return df, x_part


def get_means_and_stds(x_part: pd.DataFrame,
                       means: Optional[np.ndarray],
                       stds: Optional[np.ndarray]) -> \
        Tuple[Optional[np.ndarray], Optional[np.ndarray]]:
    """
    @param x_part:
    @param means:
    @param stds:
    @return:
    """
    if means is None:
        means = np.mean(x_part.values, axis=0)
    if stds is None:
        stds = np.std(x_part.values, axis=0)
    return means, stds


def train_nn(x_train: np.ndarray, y_train: np.ndarray,
             x_valid: np.ndarray, y_valid: np.ndarray,
             x_test: np.ndarray, is_rnn: bool, batch_size: int,
             epochs: int) -> OutputTrainData:
    """
    @param x_train:
    @param y_train:
    @param x_valid:
    @param y_valid:
    @param x_test:
    @param is_rnn:
    @param batch_size:
    @param epochs:
    @return:
    """
    shape = (x_train.shape[1], x_train.shape[2])
    model = get_rnn_model(shape) if is_rnn else get_conv1d_model(shape)
    model_path = f"models/model_temp{str(uuid.uuid4())[:6]}.h5"
    logger.info("temp model path is %s", model_path)
    checkpoint = ModelCheckpoint(model_path,
                                 verbose=0, save_best_only=True,
                                 monitor='val_mae', save_weights_only=False,
                                 mode='auto')
    history = model.fit(
        x_train, y_train,
        batch_size=batch_size,
        epochs=epochs,
        verbose=0,
        validation_data=(x_valid, y_valid), callbacks=[checkpoint,
                                                       CustomMlflowCallback()])
    logger.info(
        "training valid history:\n %s", pd.DataFrame.from_dict(history.history))
    model = models.load_model(model_path)

    return OutputTrainData(
        model=model,
        predictions_for_test=model.predict(x_test, verbose=0).reshape(-1,))


def process_training_nn(train_df: pd.DataFrame, valid_df: pd.DataFrame,
                        test_df: pd.DataFrame, output_folder: str,
                        is_rnn: bool, input_deep: int, forecast_deep: int,
                        batch_size: int, epochs: int):
    """
    @param train_df:
    @param valid_df:
    @param test_df:
    @param output_folder:
    @param is_rnn:
    @param input_deep:
    @param forecast_deep:
    @param batch_size:
    @param epochs:
    @return:
    """
    model_type = "rnn" if is_rnn else "conv1d"
    for target_number, target_name in enumerate(TARGET_COLUMNS):
        logger.info("prepare data for %s model, for target_number %s, "
                    "for forecast_deep %s and input_deep %s",
                    model_type, target_number, forecast_deep, input_deep)
        x_train, y_train, means, stds = \
            prepare_nn_data(
                df=train_df.copy(),
                target_number=target_number,
                input_deep=input_deep,
                deep=forecast_deep)
        logger.info("x_train shape is %s", x_train.shape)
        x_valid, y_valid, _, _ = \
            prepare_nn_data(
                df=valid_df.copy(),
                target_number=target_number,
                input_deep=input_deep,
                deep=forecast_deep,
                means=means,
                stds=stds)
        logger.info("x_valid shape is %s", x_valid.shape)
        x_test, y_test, _, _ = \
            prepare_nn_data(
                df=test_df.copy(),
                target_number=target_number,
                input_deep=input_deep,
                deep=forecast_deep,
                means=means,
                stds=stds)
        logger.info("x_test shape is %s", x_test.shape)

        logger.info("train %s model, for target_number %s, "
                    "for forecast_deep %s and input_deep %s",
                    model_type, target_number, forecast_deep, input_deep)
        model_name = \
            f"{model_type}_{input_deep}_fd{forecast_deep}_{target_number}"
        mlflow.set_experiment(model_name)
        np.savez(f"models/means_stds_for_{model_name}.npz", means, stds)
        with mlflow.start_run():
            mlflow.log_artifact(f"models/means_stds_for_{model_name}.npz")
            output_train_data = \
                train_nn(
                    x_train=x_train,
                    y_train=y_train,
                    x_valid=x_valid,
                    y_valid=y_valid,
                    x_test=x_test,
                    is_rnn=is_rnn,
                    batch_size=batch_size,
                    epochs=epochs)
            signature = infer_signature(x_test,
                                        output_train_data.predictions_for_test)
            metrics_dict = {'metrics': ['mae', 'mse']}
            metrics_list = []
            mae = mean_absolute_error(y_test,
                                      output_train_data.predictions_for_test)
            mse = mean_squared_error(y_test,
                                     output_train_data.predictions_for_test)
            metrics_list.extend([mae, mse])
            mlflow.tensorflow.log_model(output_train_data.model,
                                        artifact_path=model_name,
                                        signature=signature,
                                        registered_model_name=model_name)
            log_metric("mae", mae)
            log_metric("mse", mse)
            log_params({"model_type": model_type,
                        "target_number": target_number,
                        "batch_size": batch_size,
                        "epochs": epochs, "input_deep": input_deep,
                        "forecast_deep": forecast_deep})
            logger.info("model logged to mlflow at uri: %s",
                        MLFLOW_TRACKING_URI)
        metrics_dict.update({model_name: metrics_list})
        pd.DataFrame(metrics_dict).to_csv(
            f"{output_folder}/metrics_{model_name}.csv", index=False)
        # later we will have to choose the best model and make final train


@click.command()
@click.argument('dataset_folder', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path())
@click.argument('is_rnn', type=click.BOOL)
@click.argument('input_deep', type=click.INT)
@click.argument('forecast_deep', type=click.INT)
@click.argument('batch_size', type=click.INT)
@click.argument('epochs', type=click.INT)
@click.argument('is_clean_disk', type=click.BOOL)
def main(dataset_folder: str, output_folder: str,
         is_rnn: bool, input_deep: int, forecast_deep: int,
         batch_size: int, epochs: int, is_clean_disk: bool) -> None:
    """
    train nn models for exact forecast deep
    @param is_clean_disk: clean temp folder with models or not
    @param dataset_folder: folder for input datasets;
    @param output_folder: folder to save metrics;
    @param is_rnn:
    @param input_deep: deep of X;
    @param forecast_deep: deep of forecast;
    @param batch_size:
    @param epochs:
    """

    create_folder(output_folder)
    train_df = pd.read_csv(f"{dataset_folder}/train.csv")
    valid_df = pd.read_csv(f"{dataset_folder}/valid.csv")
    test_df = pd.read_csv(f"{dataset_folder}/test.csv")
    process_training_nn(train_df, valid_df, test_df, output_folder, is_rnn,
                        input_deep, forecast_deep, batch_size, epochs)

    if is_clean_disk:
        files_to_delete = [file for file in os.listdir("models")
                           if file != ".gitkeep"]
        for file in files_to_delete:
            os.remove(os.path.join("models", file))


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
