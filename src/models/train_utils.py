from typing import Tuple

import tensorflow as tf
from keras import Input
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, BatchNormalization, Flatten, \
    Dense, Dropout, LSTM
from tensorflow import keras

keras.utils.set_random_seed(1)
tf.config.experimental.enable_op_determinism()


def get_conv1d_model(input_shape: Tuple[int, int],
                     dropout_rate: float = 0.25) -> Sequential:
    """
    prepare simple universal model with n conv1d input channels for regression
    task
    @param input_shape: n_timeseries x n_features shape
    @param dropout_rate: base dropout rate
    @return: compiled, ready-to-train, conv1d model
    """

    model = Sequential()
    model.add(Conv1D(filters=32, kernel_size=int(input_shape[0]/2),
                     activation='relu', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Dropout(rate=dropout_rate))
    model.add(Conv1D(filters=64, kernel_size=max(2, int(input_shape[0]/3)),
                     activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling1D(pool_size=max(2, int(input_shape[0]/4))))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(rate=dropout_rate * 2))
    model.add(Dense(1))

    model.compile(loss='mse', optimizer='Adam', metrics=['mse', 'mae'])

    return model


def get_rnn_model(input_shape: Tuple[int, int],
                  dropout_rate: float = 0.4) -> Sequential:
    """
    prepare simple universal lstm model with chosen input channels for
    regression task
    @param input_shape: n_timeseries x n_features shape
    @param dropout_rate: base dropout rate
    @return: compiled, ready-to-train, lstm model
    """

    model = Sequential()
    model.add(Input(shape=input_shape))
    model.add(LSTM(16, return_sequences=True))
    model.add(LSTM(32))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1))

    model.compile(loss='mse', optimizer='Adam', metrics=['mse', 'mae'])

    return model
