from dataclasses import dataclass
from typing import Optional, Union

import catboost as ct
import numpy as np
from keras.engine.sequential import Sequential


@dataclass
class OutputTrainData:
    """
    the dataclass to contain all main information about train iteration
    """
    model: Union[ct.CatBoostRegressor, Sequential]
    predictions_for_test: Optional[np.ndarray] = None
    means: Optional[np.ndarray] = None
    stds: Optional[np.ndarray] = None
