import os
from dotenv import load_dotenv, find_dotenv


# Find and load environment variables from the '.env' file.
load_dotenv(find_dotenv(raise_error_if_not_found=False), verbose=True)

MLFLOW_S3_ENDPOINT_URL = os.environ["MLFLOW_S3_ENDPOINT_URL"]
AWS_ACCESS_KEY_ID = os.environ["AWS_ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = os.environ["AWS_SECRET_ACCESS_KEY"]
MLFLOW_TRACKING_URI = os.environ["MLFLOW_TRACKING_URI"]
