from typing import List

import pandas as pd
import numpy as np


def column_has_one_of_prefixes(column: str, prefixes: List[str]) -> bool:
    """
    :param column: name of column
    :param prefixes: list of prefixes to check
    :return: Exist or not prefix in of column in prefixes
    """
    for prefix in prefixes:
        if column.startswith(prefix):
            return True
    return False


ALL_FEATURE_COLUMNS = [
    "temperature_2m",
    "relativehumidity_2m",
    "dewpoint_2m",
    "apparent_temperature",
    "pressure_msl",
    "surface_pressure",
    "precipitation",
    "rain",
    "snowfall",
    "cloudcover",
    "cloudcover_low",
    "cloudcover_mid",
    "cloudcover_high",
    "shortwave_radiation",
    "direct_radiation",
    "direct_normal_irradiance",
    "diffuse_radiation",
    "windspeed_10m",
    "winddirection_10m",
    "windgusts_10m",
    "et0_fao_evapotranspiration",
    "weathercode",
    "vapor_pressure_deficit",
]


UNIVERSAL_FEATURE_COLUMNS = [
    "temperature_2m",
    "relativehumidity_2m",
    "pressure_msl",
    "precipitation",
    "windspeed_10m",
    "winddirection_10m"
]

TARGET_COLUMNS = ["pm2_5", "pm10"]

NN_FEATURE_COLUMNS = list(set(ALL_FEATURE_COLUMNS) -
                          set(UNIVERSAL_FEATURE_COLUMNS))


def clear_nans(dataframe: pd.DataFrame, threshold: float) -> pd.DataFrame:
    """
    function dropping nan-filled columns by threshold
    @param dataframe: source df
    @param threshold: nan-filled threshold
    @return: cleaned df
    """
    columns_to_drop = [column for column in dataframe.columns
                       if np.mean(dataframe[column].isnull()) >= threshold
                       and column not in TARGET_COLUMNS]
    dataframe = dataframe.drop(columns=columns_to_drop)
    return dataframe
