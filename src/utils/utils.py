import os
import pandas as pd


def create_folder(path: str) -> None:
    """
    function creating folder if it is not existing
    @param path: path of a new folder
    """
    if not os.path.exists(path):
        os.makedirs(path)


def read_csv_files(filenames):
    """
    Read all file in .csv format
    @param filenames: filenames to read
    @return: list of pandas DataFrame
    """
    return [pd.read_csv(filepath, index_col=0, parse_dates=["time"],
                        date_format="%Y-%m-%dT%H:%M") for filepath in filenames]


def write_csv_files(dataframes, filenames):
    """
    Wtite all file in .csv format
    @param dataframes: DataFrames to write
    @param filenames: filenames for writing
    """
    for dataframe, filename in zip(dataframes, filenames):
        dataframe.to_csv(filename)


def parse_date(date_str: str) -> pd.Timestamp:
    """
    @param date_str: date string
    @return: timestamp
    """

    try:
        # Try parsing with the first format
        return pd.to_datetime(date_str, format="%Y-%m-%d %H:%M:%S")
    except ValueError:
        # If the first format fails, try the second format
        return pd.to_datetime(date_str, format="%Y-%m-%dT%H:%M")
